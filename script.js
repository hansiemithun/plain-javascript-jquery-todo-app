$(function () {
    $.fn.todoList = function () {
        tasks = '';
        tasksCompleted = 0;
        totalFiltered = 0;
        totalTasks = 0;
        searchVal = '';

        container = $('.container');
        task = $('.task');
        taskName = $('#task-name');
        addBtn = $('#inputGroup-add-default');
        taskList = $('#task-list');
        taskStatus = $('#task-status #completed-tasks');
        filteredStatus = $('#task-status #filtered-tasks');
        search = $('#search');
        selectAll = $('#select-all');
        selectAllChk = $('#select-all-chk');
        deleteAll = $('#delete-all');

        search.keyup(function () {
            searchVal = this.value;
            if (tasks.length) {
                filterTasks();
            }
        });

        getTasks = function () {
            if (localStorage.getItem('tasks') === null) {
                tasks = [];
            } else {
                tasks = JSON.parse(localStorage.getItem('tasks'));
            }
        };

        loadTasks = function () {
            formattedTasks = '<div class="list-group-item d-none" id="no-results">No Results Found</div>';
            formattedTasks += '<div class="list-group-item d-none" id="no-records">Task List is Empty</div>';
            $.each(tasks, function (index, task) {
                if (task.selected) {
                    formattedTasks += '<div class="list-group-item task active" id="task-' + index + '">' +
                        '<input class="task-done" type="checkbox" checked><span> ' + task.name + '</span><i title="Remove ' + task.name + '" id="delete-' + index + '" class="float-right fa fa-trash-o"></i></div>';
                } else {
                    formattedTasks += '<div class="list-group-item task" id="task-' + index + '">' +
                        '<input class="task-done" type="checkbox"><span> ' + task.name + '</span><i title="Remove ' + task.name + '"  id="delete-' + index + '"  class="float-right fa fa-trash-o"></i></div>';
                }
            });
            taskList.html(formattedTasks);
            getTaskStatus();
        };

        removeTask = function (id) {
            if (confirm('Are you sure you want to delete: ' + tasks[id].name)) {
                tasks.splice(id, 1);
                loadTasks();
            }
            return false;
        };

        getTaskStatus = function () {
            tasksCompleted = $('.task.active').length;
            totalTasks = tasks.length;
            taskStatus.html(tasksCompleted + ' out of ' + totalTasks + ' tasks completed');
            if (!totalTasks) {
                search.attr('disabled', true);
                selectAll.attr('disabled', true);
                $('#no-records').removeClass('d-none');
            } else {
                search.attr('disabled', false);
                selectAll.attr('disabled', false);
                $('#no-records').addClass('d-none');
            }
            localStorage.setItem('tasks', JSON.stringify(tasks));
            selectAllChk.prop('checked', tasks.length > 0 && tasks.length === tasksCompleted);
            if (!tasksCompleted) {
                deleteAll.attr('disabled', true);
            } else {
                deleteAll.attr('disabled', false);
            }
        };

        container.on('click', 'div.task', function (e, obj, key) {
            let rowId = $(this).attr('id');
            let refId = rowId.split('-')[1];
            let target = e.target.id.split('-')[0];

            if (target === 'delete') {
                removeTask(refId);
            }
            else {
                if ($(this).hasClass('active')) {
                    selected = false;
                    $(this).removeClass('active');
                } else {
                    selected = true;
                    $(this).addClass('active');
                }
                tasks[refId].selected = selected;
                $(':checkbox', this).prop('checked', selected);
                getTaskStatus();
            }
        });

        addBtn.click(function () {
            addTask();
        });

        addTask = function () {
            let taskVal = taskName.val();
            if ($.trim(taskVal)) {
                tasks.unshift({name: taskVal, selected: false});
                taskName.val('');
                search.val('');
                loadTasks();
                $('#task-0').hide().fadeIn();
                filteredStatus.addClass('d-none');
            }
        };

        filterTasks = function () {
            totalFiltered = 0;
            $('#task-list .task').each(function (item) {
                const rowItem = $(this).find('span').text();
                if (rowItem.toLowerCase().indexOf(searchVal.toLowerCase()) != -1) {
                    $(this).show();
                    totalFiltered++;
                } else {
                    $(this).hide();
                }
            }).promise().then(function () {
                taskNotification();
            });
        };

        taskNotification = function () {
            if (!totalFiltered) {
                $('#no-results').removeClass('d-none');
            } else {
                $('#no-results').addClass('d-none');
            }
            if (searchVal) {
                filteredStatus.removeClass('d-none');
                filteredStatus.html(totalFiltered + ' filtered out of ' + tasks.length + ' tasks');
            } else {
                filteredStatus.addClass('d-none');
            }
        };

        deleteAll.on('click', function () {
            let unselectedTasks = [];
            if (confirm('Are you sure to delete the selected records?')) {
                $.each(tasks, function (index, task) {
                    if (!task.selected) {
                        unselectedTasks.push(task);
                    }
                });
                tasks = unselectedTasks;
                loadTasks();
            }
        });

        selectAll.on('click', function (e) {
            let toggleSelection = !selectAllChk.is(':checked');
            let targetId = e.target.id;

            if (targetId == 'select-all-chk') {
                toggleSelection = !toggleSelection;
            }
            $.each(tasks, function (index, task) {
                task.selected = toggleSelection;
            });
            loadTasks();
            selectAllChk.prop('checked', toggleSelection);
        });

        init = function () {
            getTasks();
            loadTasks();
        }();

    }();
});